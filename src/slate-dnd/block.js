import * as React from 'react';
import { DragSource, DropTarget } from 'react-dnd';
import { compose } from 'recompose';
import { findDOMNode } from 'react-dom';

import { TARGET } from './const';

export const dragSource = {
	beginDrag(props, monitor, component) {
		return { key: props.children.key };
	},
	endDrag(props, monitor, component) {
		return { key: props.children.key };
	},
	canDrag(props, monitor) {
		const isNeitherDraggableNorRemovable = true;
		return isNeitherDraggableNorRemovable;
	},
};

export const getIndex = (nodes, val) => {
	if (!val) {
		return -1;
	}

	if (nodes && nodes.length) {
		for (let i = 0; i < nodes.length; i++) {
			if (nodes[i] && nodes[i].key && val === nodes[i].key) {
				return i;
			}
		}
	}

	return -1;
};

export let changing = false;

export const dragTarget = {
	hover(props, monitor, component) {
		if (changing || !props.children || !props.children.key) {
			return;
		}

		const hoverIndex = getIndex(Array.from(props.editor.value.document.nodes), props.children.key);
		if (hoverIndex === -1) {
			return;
		}

		const item = monitor.getItem();

		changing = true;

		if (!item.key && item.onHover) {
			changing = true;

			let parent = props.editor.value.document.getParent(props.children.key);
			item.onHover(hoverIndex, item, parent, props.editor.change);
			changing = false;
		} else {
			if (props.children.key === item.key) {
				changing = false;
				return;
			}

			const dragIndex = getIndex(Array.from(props.editor.value.document.nodes), item.key);

			const hoverBoundingRect = findDOMNode(component).getBoundingClientRect();
			const clientOffset = monitor.getClientOffset();
			const middle = hoverBoundingRect.bottom - hoverBoundingRect.height / 2;
			const mouseY = clientOffset.y;

			// if moving upwards and mouse position > 1/2 * height -> return ||
			// if moving downwards and mouse position < 1/2 * height -> return
			// this prevents large blocks from 'hopping' up and down
			if ((dragIndex > hoverIndex && mouseY > middle) || (dragIndex < hoverIndex && mouseY < middle)) {
				changing = false;
				return;
			}

			let parent = props.editor.value.document.getParent(item.key);
			props.editor.moveNodeByKey(item.key, parent.key, hoverIndex);
			changing = false;
		}
	},
};

export class Block extends React.Component {
	render() {
		const { connectDragSource, connectDropTarget, isDragging, children, renderBlock } = this.props;
		return connectDropTarget(renderBlock(isDragging, children, connectDragSource));
	}
}

export default compose(
	DropTarget(TARGET, dragTarget, connect => ({
		connectDropTarget: connect.dropTarget(),
	})),
	DragSource(TARGET, dragSource, (connect, monitor) => ({
		connectDragSource: connect.dragSource(),
		isDragging: monitor.isDragging(),
	}))
)(Block);
