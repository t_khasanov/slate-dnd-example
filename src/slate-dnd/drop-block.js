import * as React from "react"
import { compose } from "recompose"
import { DropTarget } from "react-dnd"
import * as PropTypes from "prop-types"
import { TARGET } from "./const"


export const target = {
    hover(props, monitor, component) {
        if (props.onHover){
            return props.onHover(props,monitor,component);
        }
    },
    drop(props, monitor, component) {
        const item = monitor.getItem();

        let editor = component.context.getEditor()

        let parent = editor
        .value
        .document
        .getParent(item.key);
        props.onDrop(item, parent, editor);

        return;
    },
    canDrop(props, monitor) {
        return props.canDrop(props, monitor);
    }
};

export class DropBlock extends React.Component {
    static contextTypes = {
        getEditor: PropTypes.func
    }
    render() {
        return this.props.connectDropTarget(this.props.children);
    }
}

export default compose(
    DropTarget(TARGET, target, connect => ({
        connectDropTarget: connect.dropTarget()
    })))(DropBlock);
