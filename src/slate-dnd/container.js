import * as React from "react"
import {DragDropContext} from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import {compose} from "recompose"
import * as PropTypes from "prop-types"


export class DragDropContainer extends React.Component {
    state = {
        editor: null
    }

    static childContextTypes = {
        setEditor: PropTypes.func,
        getEditor: PropTypes.func
    }
    getChildContext(){
        const that = this;

        return {
            getEditor: () => {
                return that.state.editor;
            },
            setEditor: (editor) => {
                that.setState({editor});
            }
        }
    }
    render(){
        return this.props.children;
    }
}

export default compose(
    DragDropContext(HTML5Backend)
)(DragDropContainer)
