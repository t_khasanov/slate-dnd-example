import * as React from "react"
import {DragSource} from "react-dnd"
import {compose} from "recompose"

import {TARGET} from "./const"

export const DragPreviewBlockSource = {
    beginDrag(props,monitor,component){
        return {
            onHover: props.onHover
        }
    },
    endDrag(props, monitor, component){
        return {
            key: props.children.key
        }
    }
}

export class DragPreviewBlock extends React.Component {

    componentDidMount() {
		if (this.props.renderPreview){
            this.props.renderPreview(this.props,this.props.connectDragPreview);
        }
	}

    render(){
        const { isDragging, connectDragSource, children } = this.props
        //const opacity = isDragging ? 0.4 : 1

        return connectDragSource(this.props.renderBlock(isDragging,children));
    }
}

export default compose(
    DragSource(TARGET,DragPreviewBlockSource,(connect,monitor) => ({
        connectDragSource: connect.dragSource(),
        connectDragPreview: connect.dragPreview(),
        isDragging: monitor.isDragging(),
})))(DragPreviewBlock)
