import React from 'react';
import PropTypes from 'prop-types';
import DnDBlock from './block';
import DragPreviewBlock from './drag-preview-block';
import DragDropContainer from './container';
import DropBlock from './drop-block';
import CheckListItem from '../CheckListItem';
export { DragPreviewBlock, DragDropContainer, DropBlock };

export class DnDProvider extends React.Component {
	static contextTypes = {
		setEditor: PropTypes.func,
	};

	componentDidMount() {
		this.context.setEditor(this.props.editor);
	}

	render() {
		return this.props.children;
	}
}

export const dnd = () => {
	function renderNode(props, editor, next) {
		switch (props.node.type) {
			case 'check-list-item': {
				return (
					<DnDBlock renderBlock={renderBlock} editor={props.editor}>
						{props}
					</DnDBlock>
				);
			}
			default:
				return next();
		}
	}

	function renderBlock(isDragging, children, connectDragSource) {
		if (children.node.type === 'check-list-item')
			return (
				<div>
					<span style={{ display: 'inline-block' }}>
						<CheckListItem connectDragSource={connectDragSource} {...children} />
					</span>
				</div>
			);
		else return children;
	}

	function renderEditor(props, editor, next) {
		const children = next();
		return (
			<DnDProvider {...props.attributes} editor={editor}>
				{children}
			</DnDProvider>
		);
	}

	return { renderNode: renderNode, renderEditor: renderEditor };
};
