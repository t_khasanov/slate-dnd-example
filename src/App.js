import React from 'react';
import { Editor } from 'slate-react';
import { Value } from 'slate';

import { dnd } from './slate-dnd';
import { DragDropContainer } from './slate-dnd';

import './App.css';

const dndValue = Value.fromJSON({
	document: {
		nodes: [
			{
				object: 'block',
				type: 'check-list-item',
				data: {
					checked: false,
				},
				nodes: [
					{
						object: 'text',
						text: 'First line of text in a paragraph.',
					},
				],
			},
			{
				object: 'block',
				type: 'check-list-item',
				data: {
					checked: false,
				},
				nodes: [
					{
						object: 'text',
						text: 'Second line of text in a paragraph.',
					},
				],
			},
		],
	},
});

const dndFreeValue = Value.fromJSON({
	document: {
		nodes: [
			{
				object: 'block',
				type: 'paragraph',
				nodes: [
					{
						object: 'text',
						text: 'A line of text in a paragraph.',
					},
				],
			},
			{
				object: 'block',
				type: 'paragraph',
				nodes: [
					{
						object: 'text',
						text: 'A line of text in a paragraph.',
					},
				],
			},
		],
	},
});

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			value: dndValue,
		};
		this.plugins = [dnd()];
	}

	onChange = ({ value }) => {
		this.setState({ value });
	};

	removeNodes = () => {
		this.setState({
			value: dndFreeValue,
		});
	};

	addNodes = () => {
		this.setState({
			value: dndValue,
		});
	};

	render() {
		return (
			<div className='App'>
				<DragDropContainer>
					<Editor value={this.state.value} onChange={this.onChange} plugins={this.plugins} />
				</DragDropContainer>
				<button onClick={this.removeNodes}>Remove dnd nodes</button>
				<button onClick={this.addNodes}>Add dnd nodes</button>
			</div>
		);
	}
}

export default App;
