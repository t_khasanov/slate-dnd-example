import React from 'react';
import styled from '@emotion/styled';

const ItemWrapper = styled('div')`
	display: flex;
	flex-direction: row;
	align-items: center;
	& + & {
		margin-top: 0;
	}
`;

const CheckboxWrapper = styled('span')`
	margin-right: 0.01em;
`;

const ContentWrapper = styled('span')`
	margin-left: 30px;
	flex: 1;
	opacity: ${props => (props.checked ? 0.666 : 1)};
	text-decoration: ${props => (props.checked ? 'line-through' : 'none')};
	&:focus {
		outline: none;
	}
`;

export default class CheckListItem extends React.Component {
	onChange = event => {
		const checked = event.target.checked;
		const { editor, node } = this.props;
		editor.setNodeByKey(node.key, { data: { checked } });
	};

	render() {
		const { attributes, children, node, readOnly } = this.props;
		const checked = node.data.get('checked');
		var indentLevel = node.data.get('indent');
		if (this.moving) {
			this.moving = false;
		}
		return (
			<ItemWrapper {...attributes} style={{ marginLeft: indentLevel * 20 + 'px' }}>
				<CheckboxWrapper contentEditable={false}>
					<label className='container'>
						<input type='checkbox' checked={checked} onChange={this.onChange} />
						{this.props.connectDragSource
							? this.props.connectDragSource(<span className='checkmark' />)
							: ''}
					</label>
				</CheckboxWrapper>
				<ContentWrapper checked={checked} contentEditable={!readOnly} suppressContentEditableWarning>
					{children}
				</ContentWrapper>
			</ItemWrapper>
		);
	}
}
